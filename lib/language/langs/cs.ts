export default {
  //// SYSTEM
  connectSuccess: "Spojení se serverem navázáno",
  connectError: "Připojení k serveru selhalo, připojuji se znovu",
  serverError: "Chyba serveru",
  save: "Uložit",
  saved: "Uloženo!",
  savingError: "Nastala chyba při ukládání",

  //// LOGIN
  pleaseLogin: "Prosím, přihlašte se",
  wrongLogin: "Chybné jméno nebo heslo",
  username: "Jméno",
  password: "Heslo",
  login: "Přihlásit se",
  logging: "Přihlašování",
  logged: "Úspěšně přihlášen",
  logout: "Odhlásit se",

  //// NAVIGATOR
  dashboard: "Nástěnka",

  general: "Obecné",
  posts: "Články",
  pages: "Stránky",

  files: "Soubory",

  //// CONTENT EDITOR
  title: "Název",
  shortname: "Krátký název",

  //// FILE BROWSER
  uploading: "Nahrávání souborů...",
  uploadSuccess: "Nahrávání souborů dokončeno",
  uploadFail: "Nahrávání souborů selhalo",
  uploadFolders: "Nahrát složky",
  uploadFiles: "Nahrát soubory",
  createFolder: "Vytvořit složku",
  parentFolder: "Nadřazená složka",
  noFiles: "Žádné soubory",
  deSelect: "(Od)Vybrat",
  deSelectAll: "(Od)Vybrat vše",
  deleteSelection: "Smazat vybrané",
  delete: "Smazat",
  rename: "Přejmenovat",
};
