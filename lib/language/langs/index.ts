const LANGS = [
  "cs",
];

export type Lang = Record<string, string>;

const langs: Record<string, () => Promise<Lang>> = {};
for (const name of LANGS) {
  langs[name] = () => import("./" + name).then(a => a.default);
}

export default langs;
