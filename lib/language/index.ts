import EventEmitter from "eventemitter3";
import { useState, useEffect } from "react";
import langs from "./langs";
import type { Lang } from "./langs";

class Language extends EventEmitter {
  private lang: Lang = {};
  langs = Object.keys(langs);
  currentLang?: string;
  readonly langPromise = new Promise<string>(resolve => this.once("langChanged", resolve));

  constructor() {
    super();

    if (process.browser) {
      this.currentLang = this.langs[0];
      // Get saved language
      let currentLang = window.localStorage.getItem("locale");
      if (!currentLang) {
        // Fallback navigator's language
        currentLang = window.navigator.language.split("-")[0];
      }
      if (lang) this.currentLang = currentLang;
    }

    this.loadLang();
  }

  translate(str: string, def?: string) {
    return this.lang[str.decapitalize()] ?? def ?? str;
  }

  setLang(locale: string) {
    if (process.browser) window.localStorage.setItem("locale", locale);
    this.currentLang = locale;
    this.loadLang();
  }

  useLang() {
    const [locale, setLang] = useState(this.currentLang);

    useEffect(() => {
      this.on("langChanged", setLang);

      return () => {
        this.off("langChanged", setLang);
      };
    }, []);

    const translate = this.translate;
    return [translate.bind(this), locale] as [typeof translate, string];
  }

  private async loadLang() {
    if (!this.currentLang) return;
    this.lang = await langs[this.currentLang]();
    this.emit("langChanged", this.currentLang);
  }
}

const lang = new Language();
export default lang;
export const __ = lang.translate.bind(lang);
export const useLang = lang.useLang.bind(lang);
