declare global {
  interface String {
    capitalize(): string;
    decapitalize(): string;
  }
}

String.prototype.capitalize = function() {
  return this[0].toUpperCase() + this.slice(1);
};

String.prototype.decapitalize = function() {
  return this[0].toLowerCase() + this.slice(1);
};

function getVideoLength(length: number, float = false) {
  const hours = Math.trunc(length / 3600);
  const minutes = Math.trunc((length - hours * 3600) / 60);
  const seconds = length - minutes * 60 - hours * 3600;

  return [
    minutes && hours,
    minutes || "00",
    float ? seconds : Math.round(seconds),
  ].filter(v => v).map(v => String(v).padStart(2, "0")).join(":");
}

export {
  getVideoLength,
};
