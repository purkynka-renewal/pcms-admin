import type { Socket } from "socket.io-client";
import EventEmitter from "eventemitter3";

export default class ProxySocket extends EventEmitter {
  readonly queue: [string, ...any][] = [];
  private socket?: Socket;

  bindSocket(socket?: Socket) {
    this.socket = socket;

    if (socket) {
      // Proxy all events
      socket.onAny((eventName: string, ...args: any[]) => {
        this.emit(eventName, ...args);
      });

      // Run command queue
      while (this.queue.length) {
        socket.emit(...this.queue.shift()!);
      }
    }
  }

  send(eventName: string, ...args: any[]) {
    if (this.socket) this.socket.emit(eventName, ...args);
    else this.queue.push([eventName, ...args]);
  }

  sendOptionally(eventName: string, ...args: any[]) {
    if (this.socket) this.socket.emit(eventName, ...args);
  }
}
