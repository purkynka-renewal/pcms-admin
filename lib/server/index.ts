import io from "socket.io-client";
import EventEmitter from "eventemitter3";
import type { Socket } from "socket.io-client";
import ProxySocket from "./ProxySocket";
import DelayedCall from "./DelayedCall";
import * as modules from "./modules/";


const REINIT_DELAY = 1000;
const MAX_CACHE_SIZE = 32;


export * from "./modules/";
export { DelayedCall };

type Callback<Type> = (...args: Type[]) => void;

let onConnect: (socket: Socket) => void;

class ServerConnector extends EventEmitter {
  readonly files = modules.files;
  readonly site = modules.site;
  readonly system = modules.system;

  private readonly cache = new Map<string, any>();
  private readonly delays = new Map<string, DelayedCall<any>>();
  private readonly debounce = new Map<string, Promise<any>>();
  private socket?: Socket;
  connected = false;
  sessionID?: string = process.browser && window.localStorage.getItem("sessionID") || undefined;
  connectPromise: Promise<Socket> = new Promise(resolve => onConnect = resolve);
  connection = new ProxySocket();

  reset(): void {
    this.cache.clear();
    this.delays.clear();
    this.debounce.clear();

    if (this.socket && this.socket.connected) this.socket.disconnect();
  }

  init(): void {
    if (this.socket) return;
    const url = new URL(process.env.NEXT_PUBLIC_BACKEND!);
    this.socket = io(url.origin, {
      path: url.pathname.replace(/\/$/, "") + "/socket",
      rememberUpgrade: true,
      transports: ["websocket", "polling"],
      closeOnBeforeunload: true,
      query: {
        sessionID: encodeURIComponent(this.sessionID ?? ""),
      },
    });
    this.connection.bindSocket(this.socket);

    this.socket.on("connect", () => {
      if (this.connected) return; // duplicate

      this.emit("connect");
      this.connected = true;
      onConnect(this.socket!);
    });

    this.socket.on("connect_error", this.emit.bind(this, "connect_error"));

    this.socket.on("disconnect", reason => {
      this.emit("disconnect");

      this.connection.bindSocket();
      this.connected = false;
      this.connectPromise = new Promise(resolve => onConnect = resolve);

      if (reason === "io server disconnect") {
        // Fatal error happened, let's boldly reconnect
        this.socket!.connect();
      } else {
        // Something else happened, reset is probbably needed
        this.socket!.off();
        this.socket = undefined;
        setTimeout(() => this.init(), REINIT_DELAY);
      }
    });

    this.socket.on("sys:session", (sessionID: string) => {
      this.sessionID = sessionID;
      window.localStorage.setItem("sessionID", sessionID);
    });
  }

  private hashCall(eventName: string, ...args: any[]) {
    return JSON.stringify([eventName, ...args]); // NOTE: There might be a better hashing method out there?
  }

  async _waitFor<Type>(eventName: string, ...data: any[]): Promise<Type> {
    const hash = this.hashCall(eventName, ...data);

    // If this exact command is already running at this moment, don't rerun in
    if (this.debounce.has(hash)) return this.debounce.get(hash);

    const promise = new Promise<Type>((resolve, reject) =>
      this.connection.send(eventName, ...data, (response: Type, error: string) => {
        this.debounce.delete(hash);

        if (error) {
          reject(error);
          this.emit("error", error);
        }
        else resolve(response);

        // Trigger update events
        this.emit(hash, response);
        this.cache.set(hash, response);
        this.cleanCache();
      })
    );
    this.debounce.set(hash, promise);

    return promise;
  }

  cleanCache() {
    if (this.cache.size > MAX_CACHE_SIZE) cutMap(this.cache, MAX_CACHE_SIZE);
    if (this.delays.size > MAX_CACHE_SIZE) cutMap(this.delays, MAX_CACHE_SIZE);
    if (this.debounce.size > MAX_CACHE_SIZE) cutMap(this.debounce, MAX_CACHE_SIZE);
  }

  async call<Type>(eventName: string, ...args: any[]) {
    return this._waitFor<Type>(eventName, ...args);
  }

  slowCall<Type>(delay: number, eventName: string, ...args: any[]): DelayedCall<Type> {
    let delayedCall = this.delays.get(eventName);

    if (!delayedCall) {
      delayedCall = new DelayedCall<Type>(
        delay,
        () => this.delays.delete(eventName),
        (..._args) => this._waitFor<Type>(eventName, ..._args)
      );

      this.delays.set(eventName, delayedCall);
    }

    delayedCall.touch(args);

    return delayedCall;
  }

  getCachedData<Type>(eventName: string, ...args: any[]) {
    return this.cache.get(this.hashCall(eventName, ...args)) as Type;
  }

  // Register a callback for when other part of code refreshes data that your part uses
  getCallListener<Type>(callback: Callback<Type>, eventName: string, ...args: any[]): () => void {
    const hash = this.hashCall(eventName, ...args);

    this.on(hash, callback);

    return () => {
      this.off(hash, callback);
    };
  }


  getFileURL(file: modules.File, thumbnail?: boolean) {
    if (thumbnail && file.media && file.media.type === "VIDEO" && !file.media.thumbnail) return;
    return (
      process.env.NEXT_PUBLIC_BACKEND! +
      "/files/" +
      (thumbnail && file.media && file.media.thumbnail || file.id + "." + file.ext)
    );
  }

  getImage(fileId: string, size: number) {
    return (
      process.env.NEXT_PUBLIC_BACKEND! +
      "/images/" +
      size + "/" +
      fileId + ".webp"
    );
  }

  normalizePath(path: string) {
    return ("/" + path.trim() + "/").replace(/\/+/g, "/");
  }
}

export default new ServerConnector();


function cutMap(map: Map<any, any>, size: number) {
  const iterator = map.keys();
  while (map.size > size) map.delete(iterator.next());
}
