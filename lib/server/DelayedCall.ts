export default class DelayedCall<Type> extends Promise<Type>  {
  private readonly delay: number;
  private readonly destroy: () => void;
  private readonly resolve: (result: Type) => void;
  private readonly reject: (reason: any) => void;
  private args: any[];
  private timeout?: number;
  callee: (...args: any[]) => Type | Promise<Type>;
  retouched: boolean;

  constructor(
    delay: number,
    destroy: () => void,
    callee: (...args: any[]) => Type | Promise<Type>,
  ) {
    let _resolve: (result: Type) => void;
    let _reject: (reason: any) => void;
    super((resolve, reject) => {
      _resolve = resolve;
      _reject = reject;
    });

    this.delay = delay;
    this.destroy = destroy;
    this.retouched = false;
    this.resolve = _resolve!;
    this.reject = _reject!;
    this.callee = callee;
    this.args = [];
    this.catch(reason => {
      if (reason !== "cancelled") throw reason;
      return null;
    });
  }

  static get [Symbol.species]() {
    return Promise;
  }

  cancel() {
    if (this.timeout) clearTimeout(this.timeout);
    this.reject("cancelled");
  }

  touch(args: any[]) {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.retouched = true;
    }

    this.args = args;
    this.timeout = window.setTimeout(this.trigger.bind(this), this.delay);
  }

  forceTrigger() {
    if (this.timeout) clearTimeout(this.timeout);
    this.trigger();
    return this;
  }

  private async trigger() {
    this.destroy();

    const result = await this.callee(...this.args);
    this.resolve(result);
  }
}
