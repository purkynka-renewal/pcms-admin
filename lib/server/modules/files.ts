import useServiceData from "hooks/useServiceData";
import server from "lib/server";

export type Folder = {
  id: number;
  name: string;
  files: File[];
  folders: Folder[];

  parent?: Folder;
  parentId?: number;
};

type FileBase = {
  id: string;
  name: string;
  ext: string;
  mime: string;
  size: number;
};

export type MediaField = {
  type: "IMAGE" | "VIDEO";
  width: number;
  height: number;
  length: number | null;
  label: string | null;
  thumbnail: string | null;
};

export type Media = FileBase & { media: MediaField };
export type File = FileBase & { media: MediaField | null };
export type FileNode = File | Folder;

export const files = {
  get(id: string) {
    return server.call<File | null>("files:get", id);
  },
  useFile(id: string) {
    return useServiceData<File | null>("files:get", id);
  },

  useImageSizes() {
    return useServiceData<number[]>("files:getImageSizes");
  },

  getFolder(pathOrId: string | number) {
    return server.call<Folder>("files:list", pathOrId);
  },
  useFolder(pathOrId: string | number) {
    return useServiceData<Folder>("files:list", pathOrId);
  },

  rename(id: string | number, name: string) {
    return server.call<void>("files:rename", id, name);
  },

  mkdir(path: string) {
    return server.call<{ id: number }>("files:mkdir", path);
  },

  rmdir(pathOrId: string | number) {
    return server.call<boolean>("files:rmdir", pathOrId);
  },

  rm(id: string | number) {
    return server.call<boolean>("files:rm", id);
  },
};
