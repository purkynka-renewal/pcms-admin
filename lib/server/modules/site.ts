import useEditableContent from "hooks/useEditableContent";
import server from "lib/server";

export type Site = {
  id: number;
  name: string;
  shortname: string;
  description: string;
  about: string;
  email: string;
};

export const site = {
  get() { return server.call<Site>("content:get", "site"); },

  useEdit(onSave: (result: any, error?: any) => void) {
    return useEditableContent<Site>("site", onSave);
  },
};
