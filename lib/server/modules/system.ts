import server from "lib/server";

export const system = {
  async login(login: string, password: string): Promise<boolean> {
    return server.call("sys:login", login, password);
  },

  async logout(): Promise<void> {
    return server.call("sys:logout").then(() => {
      server.reset(); // reset connection
    });
  },
};
