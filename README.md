# PCMS admin panel

## Requirements
- PCMS

## Instalation
- Install and run PCMS
- Clone the repo
- Rename/copy `.env.sample` to `.env` and modify the vars for your needs
- Run `npm ci` to install dependencies
- Run `npm run build` to build and export the app
- Done

Run with either `npx next start` or serve the `build/` directory.

Example NGINX configuration with pcms-admin installed in `/var/www/pcms-admin/`:
```nginx
location /pcms {
  rewrite ^(.+)/$ $1; # Slash at the end causes problems     
  alias /var/www/pcms-admin/build/; # Root for the files
  try_files $uri $uri/ $uri.html =404; # Try to add .html extension
  error_page 404 /pcms/404.html; # 404 error page

  location /pcms/files {
    root /var/www/pcms-admin/files/;
    try_files /[[...path]].html =404; # Only try `[[...path]].html`
  }
}
```
