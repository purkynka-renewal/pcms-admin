module.exports = {
  reactStrictMode: true,
  basePath: process.env.BASE_PATH || "",
  images: {
    domains: ["localhost", process.env.NEXT_PUBLIC_BACKEND_DOMAIN],
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
};
