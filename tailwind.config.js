const colors = require('tailwindcss/colors');

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}', './hooks/**/*.{js,ts,jsx,tsx}'],
  darkMode: "class", // 'media' or 'class'
  theme: {
    extend: {
      colors: {
        pgreen: {
          DEFAULT: "#199F58"
        },
        pred: {
          DEFAULT: "#E2001A"
        },
        pblue: {
          DEFAULT: "#003E90"
        },
        gray: colors.gray,
      },
    },
    fontFamily: {
      sans:  'ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, Lato, Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Noto Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
      serif: 'ui-serif, Georgia, Cambria, "Times New Roman", Times, serif',
      mono:  'ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace',
    },
    tooltipArrows: {
      'popup-arrow': {
        border: 0,
        borderColor: "transparent",
        backgroundColor: colors.gray[700],
        size: 10,
        offset: -7.5,
      },
    },
  },
  variants: {
    extend: {
      wordBreak: ["group-hover"],
      overflow: ["group-hover"],
      height: ["group-hover"],
      whitespace: ["group-hover"],
      brightness: ["group-hover"],
      borderWidth: ["last"],
      borderRadius: ["last", "first"],
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography"),
    require('tailwindcss-tooltip-arrow-after')(),
  ],
};
