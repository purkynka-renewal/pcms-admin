export default function LoadingScreen() {
  return (
    <div className="w-full h-full flex items-center justify-center">
      <span className="inline-block text-6xl text-pgreen font-normal fa-pulse">P</span>
    </div>
  );
}
