import { Component, createRef } from "react";
import dynamic from "next/dynamic";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
import { toast } from "react-hot-toast";
import cn from "classnames";
import lang, { __ } from "lib/language";
import server, { FileNode } from "lib/server";
import Dialog from "$elements/Dialog";

const FileItem = dynamic(() => import("./FileItem"));
const FileContextMenu = dynamic(() => import("./FileContextMenu"));

type FileSelectorProps = {
  files?: FileNode[] | null;
  cd: (folder: string) => void;
  path: string | null;
  reff?: { current?: FileSelector | null };
  setSelected?: (selected: (string | number)[]) => void;
};

type FileSelectorState = {
  selection: (string | number)[];
  fileContext: FileNode | null;
  dropping: boolean;
};

export default class FileSelector extends Component<FileSelectorProps, FileSelectorState> {
  id = "file-selector-" + Math.random();
  ref = createRef<HTMLDivElement>();
  lastSelect?: string | number;
  state: FileSelectorState = {
    selection: [],
    dropping: false,
    fileContext: null,
  };
  listeners: Record<string, (...props: any[]) => any> = {};

  constructor(props: FileSelectorProps) {
    super(props);
    if (props.reff) props.reff.current = this;
  }

  componentDidMount() {
    lang.on("langChanged", this.listeners.lang = () => this.setState({}));
    this.ref.current!.addEventListener("keydown", this.listeners.keyboard = onKeyboard.bind(this));
  }

  componentWillUnmount() {
    lang.off("langChanged", this.listeners.lang);
    this.ref.current!.removeEventListener("keydown", this.listeners.keyboard);
  }

  componentDidUpdate(oldProps: FileSelectorProps) {
    if (this.props.path !== oldProps.path) {
      this.select(null);
      this.ref.current!.scrollTo({ top: 0, behavior: "smooth" });
      this.ref.current!.focus();
    }
  }

  onDrop(ev: React.DragEvent) {
    if (!ev.dataTransfer.files.length || !this.props.path) return;
    uploadFiles(ev.dataTransfer.files, this.props.path);
    ev.preventDefault();
    this.setState({ dropping: false });
  }

  onDragOver(ev: React.DragEvent) {
    if (!ev.dataTransfer.types.includes("Files") || !this.props.path) return;
    ev.dataTransfer.dropEffect = "move";
    ev.preventDefault();
    if (!this.state.dropping) this.setState({ dropping: true });
  }

  onContextMenu(ev: React.MouseEvent) {
    const run = () => Dialog.toggle(this.id, true, ev);

    // Right-clicked a file
    if (ev.isDefaultPrevented()) return run();

    // Right-clicked elsewhere
    this.setState({ fileContext: null }, () => {
      ev.preventDefault();
      run();
    });
  }

  onFileContextMenu(ev: React.MouseEvent, file: FileNode) {
    ev.preventDefault();
    if (this.state.selection.length <= 1) this.select(file.id, false, true);
    this.setState({ fileContext: file });
  }

  render() {
    const { files, cd } = this.props;
    const { selection, dropping, fileContext } = this.state;

    return (
      <div tabIndex={0} ref={this.ref} className="relative h-full"
        onClick={ev => ev.isDefaultPrevented() || this.select(null)}
        onDrop={this.onDrop.bind(this)}
        onDragOver={this.onDragOver.bind(this)}
        onDragLeave={() => this.setState({ dropping: false })}
        onContextMenu={this.onContextMenu.bind(this)}
      >
        <div className={cn(
          "absolute inset-0 text-8xl flex justify-center items-center bg-black z-20 transition pointer-events-none",
          dropping ? "opacity-75" : "opacity-0",
        )}><FontAwesomeIcon icon={faUpload} /></div>

        <div className="bg-black bg-opacity-25 rounded h-full overflow-y-auto">
          {files && (files.length ? (
            <ul className="grid gap-2 grid-cols-2 sm:grid-cols-3 lg:grid-cols-5 xl:grid-cols-6 xl2:grid-cols-7 xl3:grid-cols-8 p-2 pb-16">
              {files.map(file =>
                <li key={file.id}>
                  <FileItem data-id={file.id}
                    file={file} selected={selection.includes(file.id)}
                    onClick={ev => onSelect.apply(this, [ev, file.id])}
                    onDoubleClick={!("size" in file) ? () => cd(file.name) : undefined}
                    onContextMenu={ev => this.onFileContextMenu(ev, file)}
                  />
                </li>
              )}
            </ul>
          ) : (
            <div className="w-full h-full flex flex-col justify-center items-center text-2xl opacity-50">
              <FontAwesomeIcon icon={faFolderOpen} className="text-8xl" />
              <span>{__("noFiles")}</span>
            </div>
          ))}
        </div>

        <FileContextMenu selector={this} selection={selection} fileContext={fileContext} />
      </div>
    );
  }

  static upload = upload;

  open(id?: string | number) {
    if (!this.props.files) return false;
    const { selection } = this.state;
    id = id === undefined && selection.length === 1 ? selection[0] : id;
    if (!id === undefined) return false;

    const target = this.props.files.find(item => item.id === id)!;

    // If directory, then enter it
    if (!("size" in target)) this.props.cd(target.name);

    return true;
  }

  // WARNING: Causes "This page is slowing down browser" when used on many files
  delete(ids?: (string | number)[]) {
    if (!this.props.files) return false;
    for (const id of ids ?? this.state.selection) {
      if (!this.props.files.some(file => file.id === id)) continue;
      server.files.rm(id);
    }
    return true;
  }

  select(id: string | number | (string | number)[] | null, additive?: boolean, forceAdd?: boolean) {
    const { selection } = this.state;
    if (!id) {
      if (!selection.length) return;
      selection.length = 0;
    }

    else if (!Array.isArray(id)) {
      if (additive) {
        if (selection.includes(id)) {
          if (forceAdd) return;
          selection.splice(selection.indexOf(id), 1);
        }
        else selection.push(id);
      } else {
        if (selection.includes(id) && selection.length == 1) return;

        selection.length = 0;
        selection.push(id);
      }
    }
    else {
      let changed = false;
      for (const _id of id) {
        if (selection.includes(_id)) {
          if (forceAdd) continue;
          selection.splice(selection.indexOf(_id), 1);
        }
        else selection.push(_id);
        changed = true;
      }
      if (!changed) return;
    }
    this.setState({ selection });
    this.lastSelect = selection[selection.length - 1];
    if (this.props.setSelected) this.props.setSelected([...selection]);
  }

  toggleSelectAll() {
    if (this.state.selection.length) this.select(null);
    else if (this.props.files) this.select(this.props.files.map(file => file.id));
  }
}


function onKeyboard(this: FileSelector, ev: KeyboardEvent) {
  const { files, cd } = this.props;

  if (!files || !files.length) return;

  switch (ev.key) {
    // Enter/Open selected directory/file
    case "Enter": {
      if (this.open()) ev.preventDefault();
      break;
    }

    // Go up
    case "Backspace": {
      cd("..");
      break;
    }

    // Deselect all
    case "Escape": {
      this.select(null);
      break;
    }

    // Toggle select all
    case "a": {
      if (!ev.ctrlKey) break;
      this.toggleSelectAll();
      ev.preventDefault();
      break;
    }

    // Delete selected files
    case "Delete": {
      if (this.delete()) ev.preventDefault();
      break;
    }

    // Select next/previous file
    case "ArrowLeft":
    case "ArrowRight": {
      if (!this.lastSelect) break;
      const currentIndex = files.findIndex(item => item.id === this.lastSelect);
      const direction = ev.key === "ArrowLeft" ? -1 : 1;
      const nextId = files[(files.length + currentIndex + direction) % files.length].id;
      onSelect.apply(this, [ev, nextId]);
      break;
    }
  }
}


function onSelect(this: FileSelector, ev: React.MouseEvent | KeyboardEvent, id: string | number) {
  const { files } = this.props;
  if (!files) return;

  if ("key" in ev) ev.stopPropagation();
  ev.preventDefault();

  // shift-select
  if (ev.shiftKey) {
    const lastId = this.lastSelect;
    if (!lastId || id === lastId) return this.select(id);

    const startIndex = files.findIndex(val => val.id === lastId);
    const endIndex = files.findIndex(val => val.id === id);
    const direction = Math.sign(endIndex - startIndex);
    const toSelect = [];
    for (let i = startIndex + direction; i != endIndex; i += direction) {
      toSelect.push(files[i].id);
    }
    this.select([...toSelect, id], true);
  }
  // normal/ctrl-select
  else this.select(id, ev.ctrlKey);
}

export function upload(path = "/", isFolder = false) {
  const input = document.createElement("input");
  input.setAttribute("type", "file");
  input.setAttribute("multiple", "");
  if (isFolder) input.setAttribute("webkitdirectory", "");

  return new Promise(resolve => {
    input.onchange = ev => {
      ev.preventDefault();
      const files = (ev.target as HTMLInputElement).files;
      const promise = uploadFiles(files, path).then(resolve);

      toast.promise(promise, {
        loading: __("uploading"),
        success: __("uploadSuccess"),
        error: __("uploadFail"),
      }, {
        duration: 10_000,
      });
    };
    input.click();
  });
}

export async function uploadFiles(files: FileList | null, path = "/") {
  if (!files) return;
  const body = new FormData();

  for (const file of files) {
    body.append("files", file, window.btoa(escape(path + "/" + ((file as any).webkitRelativePath || file.name))));
  }

  return fetch(process.env.NEXT_PUBLIC_BACKEND + "/files/?sessionID=" + encodeURIComponent(server.sessionID || ""), {
    method: "POST",
    body,
  });
}
