import { useState, useEffect, useMemo, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolder, faArrowUp, faFile, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { faSquare, faCheckSquare } from "@fortawesome/free-regular-svg-icons";
import cn from "classnames";
import { useLang } from "lib/language";
import server from "lib/server";
import CreateFolder from "./CreateFolder";
import FileSelector from "./FileSelector";

type FileBrowserProps = {
  path?: string; // If defined, switches to controlled mode
  onChange?: (path: string) => void;
};

export default function FileBrowser({ path: aPath, onChange }: FileBrowserProps) {
  const [__] = useLang();
  const [selected, setSelected] = useState<(string | number)[]>([]);
  const [path, _setPath] = useState(aPath ?? "/");
  const [folder] = server.files.useFolder(path);
  const files = useMemo(() => folder ? [...folder.folders!, ...folder.files!] : null, [folder]);
  const dirs = path.split("/").filter(v => v);
  const fileSelector = useRef<FileSelector>(null);

  useEffect(() => {
    if (aPath && path !== aPath) _setPath(aPath);
  }, [path, aPath]);


  const _upload = (folder = false) => {
    FileSelector.upload(path, folder);
  };
  const setPath = (path: string) => {
    if (onChange) onChange(path);
    if (!aPath) _setPath(path);
  };

  const select = !!selected.length;

  return (
    <div className="h-full w-full overflow-hidden flex flex-col">
      <div className="flex flex-wrap items-center mb-2">
        <button className="p-2 px-3 bg-gray-800 rounded mr-2" onClick={() => setPath("/" + dirs.slice(0, -1).join("/"))} title={__("parentFolder")}>
          <FontAwesomeIcon icon={faArrowUp} />
        </button>

        <ul className="block">
          {["/", ...dirs].map((dir, i) =>
            <li key={dir + i} className={cn(
              "inline-block p-2 bg-gray-800 border border-r-0 last:border-r border-gray-900",
              "first:rounded-l last:rounded-r cursor-pointer",
            )} onClick={() => setPath(server.normalizePath(dirs.slice(0, i).join("/")))}>
              {dir}
            </li>
          )}
        </ul>


        <div className="ml-auto" />

        <button className={cn("p-2 px-3 rounded mx-1 bg-gray-600 hover:bg-gray-500 transition")}
          onClick={() => fileSelector.current!.toggleSelectAll()}
          title={__("deSelectAll")}
        ><FontAwesomeIcon icon={select ? faSquare : faCheckSquare} fixedWidth /></button>
        <button className={cn("p-2 px-3 rounded mx-1 bg-gray-600 transition", select ? "hover:bg-pred" : "opacity-50")} disabled={!select}
          onClick={() => fileSelector.current!.delete()}
          title={__("deleteSelection")}
        ><FontAwesomeIcon icon={faTrashAlt} fixedWidth /></button>

        <div className="h-3/4 border-r border-px border-gray-700 mx-1" />

        <div className="mx-1 inline" title={__("createFolder")}><CreateFolder path={dirs} /></div>
        <button className="p-2 px-3 bg-pgreen rounded mx-1" onClick={() => _upload(true)} title={__("uploadFolders")}>
          <span className="fa-layers fa-fw">
            <FontAwesomeIcon icon={faFolder} />
            <FontAwesomeIcon icon={faArrowUp} inverse transform="shrink-8 down-1" className="text-pgreen" />
          </span>
        </button>
        <button className="p-2 px-3 bg-pgreen rounded ml-1" onClick={() => _upload()} title={__("uploadFiles")}>
          <span className="fa-layers fa-fw">
            <FontAwesomeIcon icon={faFile} />
            <FontAwesomeIcon icon={faArrowUp} inverse transform="shrink-8 down-1" className="text-pgreen" />
          </span>
        </button>
      </div>

      <FileSelector files={files} path={path} reff={fileSelector}
        cd={folder => setPath("/" + [...dirs, folder].join("/"))}
        setSelected={setSelected}
      />
    </div>
  );
}
