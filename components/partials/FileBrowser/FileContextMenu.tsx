import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { } from "@fortawesome/free-solid-svg-icons";
import cn from "classnames";
import Dialog from "$elements/Dialog";
import { useLang } from "lib/language";
import type { IconDefinition } from "@fortawesome/fontawesome-common-types";
import type { FileNode } from "lib/server";
import type FileSelector from "./FileSelector";

type FileContextMenuProps = {
  selector: FileSelector;
  selection: (string | number)[],
  fileContext: FileNode | null;
  onClose?: () => void;
};

type MenuItem = {
  icon?: IconDefinition;
  label: React.ReactNode;
  keystroke?: React.ReactNode;
  action: () => void;
};

export default function FileContextMenu({ selector, selection, fileContext, onClose }: FileContextMenuProps) {
  const [__] = useLang();
  const menu: MenuItem[] = [
    (fileContext ? {
      label: __("deSelect"),
      action: () => selector.select([fileContext.id], true),
    } : {
      label: __("deSelectAll"),
      action: () => selector.toggleSelectAll(),
    }),
    {
      label: __(selection.length > 1 ? "deleteSelection" : "delete"),
      keystroke: (<kbd>Delete</kbd>),
      action: () => selector.delete(),
    },
    fileContext && {
      label: __("rename"),
      keystroke: "TODO"/*(<kbd>F2</kbd>)*/,
      action: () => { /* TODO */ },
    },
  ].filter(v => v) as MenuItem[];

  const onClick = (ev: React.MouseEvent) => {
    ev.stopPropagation();
    Dialog.toggle(selector.id, false);
  };

  return (
    <Dialog id={selector.id} position="right bottom" relativeTo="mouse" onClose={onClose}>
      <ul className="rounded bg-gray-800 flex flex-col py-1" style={{ minWidth: 120 }} onClick={onClick}>
        {menu.map((item, i) =>
          <li key={i}><button className={cn(
            "block w-full px-2 py-1 flex font-normal transition hover:bg-gray-500 hover:bg-opacity-25",
          )} onClick={item.action}>

            <span>{item.label}</span>
            {item.keystroke &&
              <span className="inline-block text-sm py-1 pl-4 ml-auto">{item.keystroke}</span>
            }
          </button></li>
        )}
      </ul>
    </Dialog>
  );
}
