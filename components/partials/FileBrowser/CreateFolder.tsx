import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolderPlus } from "@fortawesome/free-solid-svg-icons";
import Popup from "$elements/Popup";
import Input from "$elements/form/Input";
import { useLang } from "lib/language";
import server from "lib/server";

export default function CreateFolder({ path }: { path: string[] }) {
  const [name, setName] = useState("");
  const [__] = useLang();

  const onSubmit = (ev: React.FormEvent) => {
    ev.preventDefault();
    server.files.mkdir([...path, name].join("/"));
    setName("");
  };

  return (
    <Popup trigger={toggle =>
      <button onClick={toggle} className="p-2 px-3 bg-pgreen rounded">
        <FontAwesomeIcon icon={faFolderPlus} fixedWidth />
      </button>
    } className="right-1/2 transform translate-x-1/2">
      <form className="flex w-48" onSubmit={onSubmit}>
        <Input value={name} onChange={({ target }) => setName(target.value)} className="py-1" placeholder={__("title")} />
        <button className="px-2 ml-1 bg-pgreen rounded">
          <FontAwesomeIcon icon={faFolderPlus} fixedWidth size="sm" />
        </button>
      </form>
    </Popup>
  );
}
