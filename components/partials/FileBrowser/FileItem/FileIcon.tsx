import { useState, useMemo } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import { faFile, faFolder, faFileVideo, faFileAudio, faFileImage, faFileCode, faFileArchive, faFilePdf, faFileAlt, faCaretRight } from "@fortawesome/free-solid-svg-icons";
import cn from "classnames";
import { File, Folder } from "lib/server";
import { getVideoLength } from "lib/utils";
import Image from "$elements/Image";

const DEFAULT_ICON = faFile;
const FOLDER_ICON = faFolder;
const FILE_ICONS: Record<string, IconDefinition> = {
  video: faFileVideo,
  audio: faFileAudio,
  image: faFileImage,
  code: faFileCode,
  archive: faFileArchive,
  pdf: faFilePdf,
  text: faFileAlt,
};

const typeOverride = {
  code: ["text/html", "text/x-shellscript"],
  archive: ["application/zip", "application/gzip", "application/x-compressed-tar", "application/vnd.rar", "application/x-7z-compressed", "application/x-freearc", "application/x-bzip", "application/x-bzip2"],
  pdf: ["application/pdf"],
};

export default function FileIcon({ file, selected }: { file: File | Folder, selected?: boolean }) {
  const [previewLoaded, setPreviewLoaded] = useState(false);
  const icon = useMemo(() => "size" in file ? getIcon(file) : FOLDER_ICON, [file]);

  return (
    <div style={{ height: 96 }} className="relative filter transition group-hover:brightness-125">
      <FontAwesomeIcon icon={icon} style={{ fontSize: 64, marginTop: 16 }}
        className={cn("transition", previewLoaded && "opacity-0", selected ? "text-pgreen" : "text-gray-200")}
      />
      {"media" in file && file.media &&
        <div className="absolute inset-0 flex justify-center items-center">
          <div className={cn("relative rounded transition p-px", selected && "ring ring-pgreen bg-pgreen")}>
            {file.media.type === "VIDEO" &&
              <>
                <FontAwesomeIcon icon={faCaretRight} size="2x" className={cn(
                  "absolute top-1/2 transform -translate-y-1/2 z-10 transition",
                  !previewLoaded && "opacity-0",
                )} />
                <div
                  className="absolute bottom-0 right-0 text-sm bg-black bg-opacity-50 text-white px-1 z-10 rounded rounded-br-none"
                  children={getVideoLength(file.media.length!)}
                />
              </>
            }

            <Image height={96} src={file} onLoad={() => setPreviewLoaded(true)}
              className={cn("pointer-events-none", file.media.type === "VIDEO" && previewLoaded && "opacity-75")}
            />
          </div>
        </div>
      }
    </div>
  );
}

function getIcon(file: File) {
  const mime = file.mime.split("/");
  let type = mime[0];
  for (const [_type, _mimes] of Object.entries(typeOverride)) {
    if (!_mimes.includes(file.mime)) continue;
    type = _type;
    break;
  }
  return FILE_ICONS[type] || DEFAULT_ICON;
}
