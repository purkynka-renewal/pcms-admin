import cn from "classnames";
import server, { FileNode } from "lib/server";
import FileIcon from "./FileIcon";

type FileItemProps = React.HTMLProps<HTMLDivElement> & {
  file: FileNode;
  selected?: boolean;
};

export default function FileItem({ file, selected, ...props }: FileItemProps) {
  const name = "size" in file ? file.name : file.name.replace(/\/$/, "").split("/").pop()!;

  const onDragStart = (ev: React.DragEvent) => {
    ev.dataTransfer.effectAllowed = "all";
    if ("size" in file) {
      const fileUrl = server.getFileURL(file) ?? "";
      ev.dataTransfer.setData("text/uri-list", fileUrl);
      ev.dataTransfer.setData("text/plain", fileUrl);
    } else {
      ev.dataTransfer.setData("text/plain", file.name);
    }
    ev.dataTransfer.setData("application/x-file-id", file.id + "");
  };

  return (
    <div onDragStart={onDragStart} draggable={true} className={cn(
      "p-2 group cursor-pointer text-center select-none",
      "rounded bg-gray-800 bg-opacity-0 transition",
    )} {...props}>
      <FileIcon file={file} selected={selected} />

      <h2 className="h-10 relative z-10">
        <div className="absolute h-full w-full rounded group-hover:h-auto">
          <span className={cn("inline-block max-w-full rounded px-2 py-1 mt-1 transition bg-transparent",
            "overflow-hidden overflow-ellipsis whitespace-nowrap text-center leading-tight",
            "group-hover:break-all group-hover:whitespace-normal group-hover:h-auto",
            selected ? "bg-pgreen bg-opacity-50" : "group-hover:bg-black group-hover:bg-opacity-40",
          )}
          >{name}</span>
        </div>
      </h2>
    </div>
  );
}
