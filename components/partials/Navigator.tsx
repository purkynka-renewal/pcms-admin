import Link from "next/link";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faColumns, faInfo, faNewspaper, faFileAlt, faBars, faTimes, faBox } from "@fortawesome/free-solid-svg-icons";
import type { IconDefinition } from "@fortawesome/fontawesome-common-types";
import cn from "classnames";
import LangSelect from "$partials/LangSelect";
import Logo from "$elements/Logo";
import { useLang } from "lib/language";
import server from "lib/server";

type NavItem = {
  icon: IconDefinition,
  title: string,
  href: string,
};

const NAV: NavItem[][] = [
  [
    {
      icon: faColumns,
      title: "dashboard",
      href: "/",
    },
  ], [
    {
      icon: faInfo,
      title: "general",
      href: "/general",
    },
    {
      icon: faNewspaper,
      title: "posts",
      href: "/posts",
    },
    {
      icon: faFileAlt,
      title: "pages",
      href: "/pages",
    },
  ],
  [
    {
      icon: faBox,
      title: "files",
      href: "/files",
    },
  ],
];

type NavgiatorProps = {
  path: string;
};

export default function Navigator({ path }: NavgiatorProps) {
  const [open, setOpen] = useState(false);
  const [__] = useLang();

  return (
    <>
      <button className={cn("block md:hidden fixed top-2 right-2 text-3xl p-2 bg-gray-900 bg-opacity-50 z-20 transition", !open && "opacity-75")}
        onClick={() => setOpen(!open)}
      >
        <FontAwesomeIcon icon={open ? faTimes : faBars} fixedWidth />
      </button>

      <header className={cn(
        "flex flex-col md:w-80 max-w-screen overflow-y-auto absolute inset-0 md:static bg-gray-900 z-10",
        "transition duration-500 transform-gpu md:transition-none md:transform-none shadow-lg md:shadow-none",
        !open && "-translate-x-full"
      )}>
        <h1 className="block px-8 py-12 text-4xl text-center">
          <Link href="/"><a onClick={() => setOpen(false)}>
            <Logo />
          </a></Link>
        </h1>

        <nav>
          <ul>
            {NAV.map((menu, i) =>
              <li key={i} className="my-6"><ul>
                {menu.map((item, j) =>
                  <li key={j}><Link href={item.href} shallow>
                    <a onClick={() => setOpen(false)} className={cn(
                      "block py-2 px-8 cursor-pointer transition hover:text-white hover:bg-gray-800 hover:bg-opacity-100 rounded m-2",
                      path === item.href ? "text-gray-200 bg-opacity-50 bg-gray-800" : "text-gray-400",
                    )}>
                      <FontAwesomeIcon icon={item.icon} fixedWidth className="mr-4" />
                      {__(item.title)}
                    </a>
                  </Link></li>
                )}
              </ul></li>
            )}
          </ul>
        </nav>

        <div className="mt-auto flex">
          <LangSelect className="m-2 mr-0" />

          <button className="p-2 m-2 flex-grow transition bg-gray-800 hover:bg-red-500 rounded"
            onClick={() => server.system.logout()}
          >
            {__("logout")}
          </button>
        </div>
      </header>
    </>
  );
}
