import cn from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLanguage } from "@fortawesome/free-solid-svg-icons";
import language from "lib/language";

type LangSelect = {
  className?: string;
  innerClass?: string;
};

export default function LangSelect({ className, innerClass }: LangSelect) {
  const [_, lang] = language.useLang();

  return (
    <label className={cn("inline-block relative", className)}>
      <FontAwesomeIcon icon={faLanguage} className="absolute top-0 left-2 h-full pointer-events-none" />
      <select className={cn("bg-gray-800 cursor-pointer pl-9 rounded", innerClass)}
        value={lang} onChange={({ currentTarget }) => language.setLang(currentTarget.value)}
      >
        {language.langs.map(item =>
          <option key={item}>{item}</option>
        )}
      </select>
    </label>
  );
}
