import { useState, useRef, useCallback } from "react";
import cn from "classnames";
import { toast } from "react-hot-toast";
import LangSelect from "$partials/LangSelect";
import Logo from "$elements/Logo";
import Input from "$elements/form/Input";
import { useLang } from "lib/language";
import server from "lib/server";

export default function LoginScreen() {
  const [error, setError] = useState(false);
  const usrInput = useRef<HTMLInputElement>(null);
  const pwdInput = useRef<HTMLInputElement>(null);
  const [__] = useLang();

  const onSubmit = useCallback((ev: React.FormEvent) => {
    ev.preventDefault();
    setError(false);
    if (!usrInput.current || !pwdInput.current) return;
    if (!usrInput.current.value || !pwdInput.current.value) return;

    toast.promise(new Promise<void>((resolve, reject) =>
      server.system.login(usrInput.current!.value, pwdInput.current!.value).then((result: boolean) => {
        setError(!result);
        result ? resolve() : reject();
      }).catch(reject)
    ), {
      loading: __("logging"),
      success: __("logged"),
      error: __("wrongLogin"),
    });
  }, [usrInput.current, pwdInput.current]);

  return (
    <div className="flex min-h-screen">
      <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 w-full max-w-sm">
        <div className="rounded p-4">
          <h1 className="text-3xl text-center mb-4 font-normal"><Logo /></h1>
          <h2 className="text-2xl text-center mb-4">{__("pleaseLogin", "Please, login.")}</h2>

          <div className={cn("text-center text-red-500 transition", !error && "opacity-0 pointer-events-none")}>
            {__("wrongLogin")}
          </div>

          <form onSubmit={onSubmit}>
            <Input label={__("username", "Username")} required type="text" ref={usrInput} placeholder="admin" name="username" outerClass="my-2" />
            <Input label={__("password", "Password")} required type="password" ref={pwdInput} placeholder="nimda" name="password" outerClass="my-2" />

            <button className="p-2 mt-8 rounded bg-pgreen w-full text-center">
              {__("login", "Login")}
            </button>
          </form>
        </div>
      </div>

      <LangSelect className="m-2 mb-3 mt-auto" />
    </div>
  );
}
