import cn from "classnames";
import LoadingScreen from "$partials/LoadingScreen";

type PageProps = {
  children?: React.ReactNode;
  loading?: boolean;
  title?: string;
  className?: string;
};

export default function Page({ children, title, loading, className }: PageProps) {
  return (
    <main className={cn(className, "pt-12 p-2 md:p-6 xl:p-12 relative flex flex-col")}>
      {title &&
        <h1 className="text-4xl md:text-5xl mb-4 xl:mb-8">{title}</h1>
      }

      {!loading && children ||
        <LoadingScreen />
      }
    </main>
  );
}
