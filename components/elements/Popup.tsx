import { useState, useRef, useEffect, useCallback } from "react";
import cn from "classnames";

type PopupPropsControlled = {
  isOpen: boolean;
  setOpen: (state: boolean) => void;
  trigger?: never;
};

type PopupPropsUncontrolled = {
  isOpen?: never;
  setOpen?: never;
  trigger: (toggle: () => void) => React.ReactNode;
};


type PopupProps = (PopupPropsControlled|PopupPropsUncontrolled) & {
  children?: React.ReactNode;
  className?: string;
};


export default function Popup({ trigger, isOpen: aIsOpen, setOpen: aSetOpen, children, className }: PopupProps) {
  const [ _isOpen, _setOpen ] = useState(aIsOpen);
  const [ isOpen, setOpen ] = trigger ? [_isOpen!, _setOpen!] : [aIsOpen!, aSetOpen!];
  const ref = useRef<HTMLDivElement>(null);

  const toggle = useCallback(() => {
    setOpen(!isOpen);
    const focusable = ref.current!.querySelector<HTMLInputElement>("input,button");
    if (focusable) focusable[isOpen ? "blur" : "focus"]();
  }, [isOpen]);

  useEffect(() => {
    const listener = () => {
      setOpen(false);
    };
    document.addEventListener("click", listener, { passive: true });
    return () => document.removeEventListener("click", listener);
  }, [setOpen]);

  return (
    <div className="relative inline-block" onClick={ ev => ev.stopPropagation() }>
      { trigger && trigger(toggle) }
      <div ref={ref} className={cn("absolute z-10 top-full transform origin-top transition", !isOpen && "scale-y-0", className)}>
        <div className="absolute left-1/2"><div className="popup-arrow-top"/></div>
        <div className={cn(
          "bg-gray-700 rounded shadow-lg p-2 right-auto",
        )}>
          { children }
        </div>
      </div>
    </div>
  );
}
