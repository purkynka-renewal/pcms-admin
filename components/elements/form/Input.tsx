import { forwardRef } from "react";
import cn from "classnames";

type InputProps = React.InputHTMLAttributes<HTMLInputElement> & {
  label?: string | JSX.Element;
  note?: string | JSX.Element;
  outerClass?: string;
  thin?: boolean;
};

export default forwardRef<HTMLInputElement, InputProps>((aProps, ref?) => {
  const { label, outerClass, note, className, ...props } = aProps;

  return (
    <label className={cn("inline-block w-full", outerClass)}>
      {label && `${label}:`}

      <input type="text" {...props} ref={ref} className={cn(
        "rounded bg-gray-800 w-full transition focus:border-pgreen focus:ring-opacity-40 focus:ring-pgreen focus:ring-4",
        className,
      )} />

      {note &&
        <small className="text-md text-gray-300">{note}</small>
      }
    </label>
  );
});
