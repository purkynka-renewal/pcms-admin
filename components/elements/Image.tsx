import { useState, useCallback, useRef, useEffect, useMemo } from "react";
import cn from "classnames";
import useIntersection from "hooks/useIntersection";
import server from "lib/server";
import type { File } from "lib/server";

type JustWidth = { src: File; width: number; height?: never; };
type JustHeight = { src: File; width?: never; height: number; };
type HeightAndWidth = { src: string | File; width: number; height: number; };

type ImageProps = Omit<React.ImgHTMLAttributes<HTMLImageElement>, "src"> & (JustWidth | JustHeight | HeightAndWidth) & {
  alt?: string;
  className?: string;

  priority?: boolean;
  onLoad?: () => void;
};

export default function Image({ src: aSrc, onLoad, priority, width, height, ...props }: ImageProps) {
  // State
  const [sizes, update] = server.files.useImageSizes();
  const [loaded, setLoaded] = useState(false);
  const [error, setError] = useState(false);
  const [setRef, isVisible] = useIntersection({});
  const ref = useRef<HTMLImageElement>(null);
  const show = priority || isVisible;

  // Scaling assistance
  if (typeof aSrc !== "string" && aSrc.media) {
    const ratio = aSrc.media.width / aSrc.media.height;
    if (height === undefined && width !== undefined) {
      height = width / ratio;
    }
    else if (width === undefined && height !== undefined) {
      width = height * ratio;
    }
  }
  const size = Math.min(width!, height!);

  useEffect(() => {
    update();
  }, []);

  // Prepare props
  const src = useMemo(() => {
    if (typeof aSrc === "string") return aSrc;
    if (!sizes) return undefined;
    let _size = size;
    let lastDistance: number | null = null;

    if (!sizes.includes(size)) {
      // Find closes possible size
      for (const possible of sizes) {
        const distance = Math.abs(possible - size);
        if (lastDistance === null || distance < lastDistance) {
          lastDistance = distance;
        } else if (distance > lastDistance) {
          _size = possible;
          break;
        }
      }
    }

    return server.getImage(aSrc.id, _size);
  }, [aSrc, size, sizes]);
  const alt = props.alt ?? "";

  // onLoad functions
  const checkLoaded = useCallback((instant = false) => {
    if (loaded || !ref.current) return;

    if (show && ref.current.complete) {
      if (instant) setLoaded(true);
      else {
        window.requestAnimationFrame(() =>
          window.requestAnimationFrame(() =>
            setLoaded(true)
          )
        );
      }

      if (onLoad && !error) onLoad();
    }
  }, [show, loaded, onLoad, ref.current]);


  useEffect(() => setRef(ref.current), [ref.current]);

  useEffect(() => {
    // Callback for too quickly loaded images
    checkLoaded(true);
  }, []);

  const attrs: React.ImgHTMLAttributes<HTMLImageElement> = {
    alt, width, height,
    className: cn(
      (!loaded || error) && "opacity-0",
      props.className,
      "block transition-opacity duration-500 overflow-hidden max-w-full max-h-full",
    ),
    onLoad: checkLoaded,
    onError: () => setError(true),
    decoding: "async",
    loading: "lazy",
    ...props,
  };

  return (
    <>
      <img ref={ref} src={show ? src : undefined} {...attrs} />

      {!isVisible &&
        <noscript><img src={src} {...attrs} /></noscript>
      }
    </>
  );
}
