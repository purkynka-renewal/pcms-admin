import useIntersection from "hooks/useIntersection";

type LazyDivProps = React.HTMLProps<HTMLElement> & {
  children?: React.ReactNode;
  as?: string;
  rootMargin?: string;
  disabled?: boolean;
};

export default function LazyElement({ as:As="div", children, disabled, rootMargin, ...props }: LazyDivProps) {
  const [ setRef, isVisible ] = useIntersection({ disabled, rootMargin });

  return (<As {...props} {...{ ref: setRef, children: isVisible && children }} />);
}
