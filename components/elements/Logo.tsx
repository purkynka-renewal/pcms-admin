export default function Logo() {
  return (
    <>
      <span className="text-pgreen font-bold">P</span>
      <span className="text-gray-300">CMS</span>
    </>
  );
}
