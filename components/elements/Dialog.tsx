import { Component, createRef } from "react";
import { createPortal } from "react-dom";
import cn from "classnames";

const instances = new Map<string, Dialog>();

type LocX = "left" | "center" | "right";
type LocY = "top" | "center" | "bottom";

type DialogProps = React.HTMLProps<HTMLDivElement> & {
  id: string;
  position: `${LocX} ${LocY}`;
  relativeTo?: "static" | "mouse" | "screen";
  positionInsideScreen?: boolean; // TODO
  onClose?: () => void;
  children: React.ReactNode | ((position: string) => React.ReactNode);
};

type DialogState = {
  open?: boolean;
  mouse: [number, number];
};

export default class Dialog extends Component<DialogProps, DialogState> {
  static toggle(id: string, state?: boolean, ev?: React.MouseEvent | MouseEvent) {
    if (instances.has(id)) instances.get(id)!.toggle(state, ev);
  }

  ref = createRef<HTMLDivElement>();
  state: DialogState = {
    open: false,
    mouse: [0, 0],
  };
  listeners: Record<string, (...props: any[]) => any> = {};

  constructor(props: DialogProps) {
    super(props);
  }

  componentDidMount() {
    instances.set(this.props.id, this);

    document.addEventListener("click", this.listeners.docClick = ev => {
      if (this.state.open && this.ref.current! !== ev.target && !this.ref.current!.contains(ev.target)) {
        this.setState({ open: false });
      }
    }, { passive: true });
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.listeners.docClick);
    instances.delete(this.props.id);
  }

  componentDidUpdate(oldProps: DialogProps, oldState: DialogState) {
    if (this.props.id !== oldProps.id) {
      instances.delete(this.props.id);
      instances.set(this.props.id, this);
    }

    if (this.props.onClose && !this.state.open && oldState.open === true) this.props.onClose();
  }

  toggle(open?: boolean, ev?: React.MouseEvent | MouseEvent) {
    return this.setState((state, props) => {
      let mouse = state.mouse;

      if (ev && props.relativeTo === "mouse") mouse = [ev.clientX, ev.clientY];

      return { open: open ?? !state.open, mouse };
    });
  }

  render() {
    const { relativeTo, position, className, children, ...props } = this.props;
    const { open, mouse } = this.state;
    const [posX, posY] = position.split(" ") as [LocX, LocY];

    // Positioning
    const pos = (relativeTo === "screen" ?
      cn(
        posX === "center" ? "left-1/2 -translate-x-1/2" : (posX === "left" ? "left-0" : "right-0"),
        posY === "center" ? "top-1/2 -translate-y-1/2" : (posY === "top" ? "top-0" : "bottom-0"),
      )
      :
      cn(
        posX === "center" ? "-translate-x-1/2" : (posX === "left" && "-translate-x-full"),
        posY === "center" ? "-translate-y-1/2" : (posY === "top" && "-translate-y-full"),
      )
    );

    const el = (
      <div {...props}
        ref={this.ref}
        style={relativeTo !== "mouse" ? undefined : { left: mouse[0], top: mouse[1] }}
        className={cn(
          className,
          "absolute z-50 transform transition",
          !open && "opacity-0 pointer-events-none",
          pos,
        )}
      >
        {/* <div className="absolute left-1/2"><div className="popup-arrow-top"/></div> */}
        {children}
      </div>
    );

    return !relativeTo || relativeTo === "static" ? el : createPortal(el, document.body);
  }
}
