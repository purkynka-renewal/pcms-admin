import { useCallback } from "react";
import { toast } from "react-hot-toast";
import Input from "$elements/form/Input";
import Page from "components/Page";
import { useLang } from "lib/language";
import server from "lib/server";
import useOnContentChange from "hooks/useOnContentChange";

export default function General() {
  const [__] = useLang();
  const { data, change, save } = server.site.useEdit(useCallback((_, error) => {
    if (error) return toast.error(__("savingError") + " " + error);
    toast.success(__("saved"));
  }, [__]));
  const onChange = useOnContentChange(change);

  return (
    <Page title={__("general")} loading={!data}>
      {data &&
        <form onSubmit={save}>
          <Input label={__("title")} value={data.name ?? ""} onChange={onChange} placeholder="admin" name="name" outerClass="my-2" />
          <Input label={__("shortname")} value={data.shortname ?? ""} onChange={onChange} placeholder="admin" name="shortname" outerClass="my-2" />
          <button>{__("save")}</button>
        </form>
      }
    </Page>
  );
}
