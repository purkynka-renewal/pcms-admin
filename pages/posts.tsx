import Page from "components/Page";
import { useLang } from "lib/language";

export default function Posts() {
  const [__] = useLang();

  return (
    <Page title={__("posts")}>
      :)
    </Page>
  );
}
