import Page from "components/Page";
import { useLang } from "lib/language";

export default function Pages() {
  const [__] = useLang();

  return (
    <Page title={__("pages")}>
      :)
    </Page>
  );
}
