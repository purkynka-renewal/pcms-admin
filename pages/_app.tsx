import App from "next/app";
import Head from "next/head";
import * as fa from '@fortawesome/fontawesome-svg-core';
import { Toaster, toast, ToastOptions } from "react-hot-toast";
import cn from "classnames";
import language, { __ } from "lib/language";
import server from "lib/server";
import LoadingScreen from "$partials/LoadingScreen";
import LoginScreen from "$partials/LoginScreen";
import Navigator from "$partials/Navigator";

import "lib/utils.ts";
import "styles/global.scss";

// Config
fa.config.autoAddCss = false;
fa.config.keepOriginalSource = false;
fa.config.observeMutations = false;

const toastOptions: ToastOptions = {
  style: {
    background: "#23272A", // notblack
    color: "white",
    maxWidth: "min(90vw, 512px)",
    minWidth: "min(90vw, 256px)",
  },
  duration: 7_500,
};

// Init
if (process.browser) server.init();

export default class CustomApp extends App {
  state = {
    authorized: false,
    connected: false,
  };

  componentDidMount() {
    server.call<boolean>("sys:isAuthorized").then(authorized => {
      this.setState({ connected: server.connected, authorized });
    });

    language.langPromise.then(() => {
      server.on("connect", () => {
        toast.success(__("connectSuccess"));
      });

      server.on("connect_error", error => {
        toast.error(`${__("connectError")} (${error.message})`);
      });

      server.on("error", error => {
        toast.error(`${__("serverError")}: ${__(error.toString())}`);
      });
    });

    server.on("connect", () => {
      this.setState({ connected: true });
    });

    server.on("disconnect", () => {
      // Reset state
      this.setState({ connected: false, authorized: false });
    });

    server.connection.on("sys:authorized", () => {
      this.setState({ authorized: true });
    });
  }

  render() {
    const { Component, pageProps, router } = this.props;

    return (
      <>
        <Head>
          <meta key="theme-color" name="theme-color" content="#199F58" />
          <meta name="robots" content="noindex, nofollow" />
          <style dangerouslySetInnerHTML={{ __html: fa.dom.css().replace(/(?<![a-z0-9])[ \n]+/gi, "") }} />
          <title>P CMS</title>
        </Head>

        <Toaster position="bottom-right" reverseOrder toastOptions={toastOptions} />

        {/* Connecting... */}
        <div className={cn("absolute inset-0 bg-gray-900 transition z-50",
          this.state.connected && "opacity-0 delay-100 pointer-events-none",
        )}>
          <LoadingScreen />
        </div>

        {/* Login please */}
        <div className={cn("absolute inset-0 bg-gray-900 transition delay-100 z-40", this.state.authorized && "opacity-0 pointer-events-none")}>
          <LoginScreen />
        </div>

        {/* Admin panel */}
        {this.state.authorized && this.state.connected &&
          <div className="w-screen h-screen flex">
            <Navigator path={router.asPath} />

            <div className="w-full h-full overflow-y-auto">
              <Component {...pageProps} router={router} />
            </div>
          </div>
        }
      </>
    );
  }
}
