import Page from "components/Page";
import { useLang } from "lib/language";

export default function Index() {
  const [__] = useLang();

  return (
    <Page title={__("dashboard")}>
      :)
    </Page>
  );
}
