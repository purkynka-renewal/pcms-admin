import { useCallback, useState, useEffect } from "react";
import dynamic from "next/dynamic";
import type { Router } from "next/router";
import Page from "components/Page";
import { useLang } from "lib/language";

const FileBrowser = dynamic(() => import("$partials/FileBrowser"));

export default function Files({ router }: { router: Router }) {
  const [__] = useLang();
  const [path, setPath] = useState<string>();

  useEffect(() => {
    setPath("/" + (router.query.path as string[] ?? []).join("/"));
  }, [router.query.path]);

  const onChange = useCallback(newPath => router.replace("/files" + newPath), [router, path]);

  return (
    <Page title={__("files")} className="overflow-hidden h-full">
      {/* { (router.query.path as string[]).join("/") } */}

      {path && <FileBrowser path={path} onChange={onChange} />}
    </Page>
  );
}
