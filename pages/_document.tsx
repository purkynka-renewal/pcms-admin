import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class CustomDocument extends Document {
  render() {
    return (
      <Html className="">
        <Head />

        <body className="text-gray-100 font-light text-lg bg-gray-900 min-h-screen overflow-x-hidden overflow-y-scroll">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
