import { useState, useEffect, useCallback, useRef } from "react";
import server from "lib/server";

export default function useServiceData<Type>(eventName: string, ...args: any[]) {
  const [data, setData] = useState<Type>(server.getCachedData(eventName, ...args));
  const debounce = useRef<Promise<Type>>();
  const firstcall = useRef(true);

  const update = useCallback<() => Promise<Type>>(
    () => {
      if (debounce.current) return debounce.current;
      const promise = server.call<Type>(eventName, ...args);
      promise.then(() => debounce.current = undefined);
      return debounce.current = promise;
    },
    [eventName, ...args],
  );

  useEffect(() => {
    const service = eventName.split(":")[0];

    // Listen for updates
    server.connection.on(service + ":updated", update);

    // Download data if none or if this is the first call
    if (!data || firstcall.current) update();
    firstcall.current = false;

    // Cleanup
    const cleanCallListener = server.getCallListener<Type>(setData, eventName, ...args);
    return () => {
      server.connection.off(service + ":updated", update);
      cleanCallListener();
    };
  }, [update, eventName, ...args]);

  return [data, update] as [Type | undefined, () => void];
}
