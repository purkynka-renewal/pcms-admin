import { useState, useEffect, useCallback, useMemo } from "react";
import server from "lib/server";

export default function useEditableContent<Type extends { [key: string]: unknown }>(type: string, onSave: (result: any, error?: any) => void, id?: any) {
  type = type.capitalize();
  // TODO: use useReducer?
  const [data, setData] = useState<Type>(server.getCachedData("content:get", type, id));
  const [editedData, setEditData] = useState<Partial<Type>>({});

  // Update-callback
  const update = useCallback(
    () => server.call("content:get", type, id),
    [type, id],
  );

  // Change-callback
  const change = useCallback(
    (field: string, value: any) => {
      if (!(data).hasOwnProperty(field)) throw new Error(`Field '${field}' doesn't exist on original data`);
      setEditData({
        ...editedData,
        [field]: value,
      });
    },
    [type, id, data, editedData],
  );

  // Save-callback
  const save = useCallback((ev?: React.FormEvent) => {
    if (ev) ev.preventDefault();

    server.call("content:set", type, editedData, id).then(onSave).catch(err => onSave(null, err));

    return false;
  }, [type, id, editedData, onSave]);

  // TODO: Apply server updates
  // useEffect(() => {
  //   setOutdated(JSON.stringify(dataCache.current) !== JSON.stringify(editedData));
  //   dataCache.current = data;
  // }, [data, editedData, dataCache]);

  // Ensure freshest data
  useEffect(() => {
    // Download data if none
    if (!data) update();

    const cancelCallListener = server.getCallListener<Type>(setData, "content:get", type, id);
    const updateListener = (aId: any) => {
      if (!id || id === aId) update();
    };
    server.connection.on("content:updated:" + type, updateListener);

    return () => {
      cancelCallListener();
      server.connection.off("content:updated:" + type, updateListener);
    };
  }, [type, id, setData, update]);

  return {
    data: useMemo<Type>(() => data && Object.assign({}, data, editedData), [data, editedData]),
    save,
    change,
    update,
  };
}
