import { useCallback, useState, useRef, useEffect } from "react";

export default function useDelayedInput<Type = any>(aValue: Type, onTrigger: (value: Type) => void | Promise<void>, delay = 2000) {
  const timeout = useRef<NodeJS.Timeout>();
  const [value, setValue] = useState(aValue);

  useEffect(() => {
    resetTimer();
    setValue(aValue);
  }, [aValue]);

  const resetTimer = () => {
    if (timeout.current) clearTimeout(timeout.current);
    timeout.current = undefined;
  };

  const trigger = async () => {
    resetTimer();
    try {
      await onTrigger(value);
    } catch (_) {
      // Reset on error
      setValue(aValue);
    }
  };

  const onChange = useCallback((ev: React.ChangeEvent<HTMLInputElement>) => {
    setValue(ev.target.value as any);

    if (timeout.current) clearTimeout(timeout.current);
    timeout.current = setTimeout(trigger, delay);
  }, [onTrigger, delay]);

  return [value, onChange, trigger] as [Type, (ev: React.ChangeEvent<HTMLInputElement>) => void, () => void];
}
