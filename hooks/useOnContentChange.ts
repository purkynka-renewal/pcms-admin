import { useCallback } from "react";

export default function useOnContentChange(onChange: (field: string, value: any) => void) {
  return useCallback((ev: React.ChangeEvent<HTMLInputElement>) => {
    onChange(ev.target.name, ev.target.value);
  }, [onChange]);
}
